Setting up for the course
=====================

Get yourself a Bitbucket account
--------------------------------

Create account if not done already ([bitbucket.org](https://bitbucket.org/)).

Free Individual account is more than enough for this course.

Note that your username will be part of your repository URLs, so choose wisely.

![Create repository](create_repo.png)

Create code repository
----------------------

You will need to create a separate repository for this course.
Choose whatever name you like, for example, 'appcrypto'.

This repository must be private and must use Git version control.

Grant the user '**info@appcrypto.lv**' (va_appcrypto) read access to your repository:

    Repository settings > User and group access

Type 'info@appcrypto.lv', press 'Enter'.

![Grant access](grant_access.png)

If you want, you may revoke the access after you get your course grade.

After the access is granted, the course instructor will have to accept the invitation
(send an email to the course instructor so that he can let you know when your invitation has been accepted).

After your invitation is accepted, add your repository URL together with student ID to the grading page at [https://appcrypto.lv/va2021/](https://appcrypto.lv/va2021/).

Start watching the course repository
-------------------------------------

Open the course repository page [https://bitbucket.org/va_appcrypto/2021/](https://bitbucket.org/va_appcrypto/2021/).

Click on the "..." on the right side of the course repository page and then "Manage notifications". Then "Watch this repository".

![Watch this repository](watch_repository.png)

Install Git client
------------------------

Command for Ubuntu:

    sudo apt-get install git

Learn how to use the client to perform basic operations.

Note that there are several GUI clients, but it is
strongly recommend *not* to use them until you get really comfortable with
command-line client and understand how it works.


Make your first commit
----------------------

Create a file called 'hello.txt' with the following content:

    My first commit

Commit and push to your repository.
Verify that it appears in the web interface under 'source' tab.

All done.

---

